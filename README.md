Fortran Project Template
========================

[![pipeline status](https://gitlab.com/everythingfunctional/fortranprojecttemplate/badges/master/pipeline.svg)](https://gitlab.com/everythingfunctional/fortranprojecttemplate/commits/master)

This is a starting point for new Fortran projects. It includes a build system,
testing framework, and all the necessary depencies. It even includes example
tests and project code, and CI setup.

To get started, run the `./Shakefile.hs` script. This will build and run the
tests. You can pass it the `--help` flag and it will tell you the commands it
accepts to do other things.

A convenience script is included for running the
tests, `tools/runSpecificTests.sh`. This script simply compiles the tests, but
then runs only the tests with descriptions that include the given string. It
reports all of the results, not just the failing ones. This is useful when
developing your tests to be sure they are testing what you think they're testing.

Note: there is a small bug in the build system. Occassionally, when you create
a new test file it will fail to recreate the driver source file, meaning your
new tests won't get included. To solve this, simply delete the file
`tests_build/vegetable_driver.f90` and run the tests again.
